import Vue from 'vue'
import Vuex from 'vuex'
// const moment = require('moment');

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        menu: [
            {nama: 'Home', icon: "house-door"},
            {nama: 'Explore', icon: "hash"},
            {nama: 'Notification', icon: "bell"},
            {nama: 'Message', icon: "envelope"},
            {nama: 'Bookmark', icon: "bookmark"},
            {nama: 'List', icon: "list"},
            {nama: 'Profile', icon: "person"},
            {nama: 'More', icon: "three-dots"},
        ],
        trends: [
            {kategori: "Music", nama: "Yuta", total: "500K"},
            {kategori: "Sport", nama: "Manchester United", total: "896K"},
            {kategori: "Businness", nama: "Ternak Lele", total: "226K"},
            {kategori: "Technology", nama: "Iphone 20", total: "1.7M"},
            {kategori: "Technology", nama: "#Samsung", total: "1M"},
            {kategori: "Sport", nama: "#MessiVsRonaldo", total: "12M"},
        ],
        tweets:[
            {
                name: "Rijwan",
                username: "@rijwan",
                image: "profile.png",
                published_at: "2022-05-14 18:55:53",
                tweet: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
                like: 30,
                retweet: 101,
                comment: 10
            },
            {
                name: "Jupri",
                username: "@jupri",
                image: "profile.jpg",
                published_at: "2022-05-14 16:55:53",
                tweet: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.",
                like: 20,
                retweet: 12,
                comment: 25
            }
        ]
    },
    mutations: {
        addTweet(state, data) {
            state.tweets.unshift(data);
        },
    },
    actions: {
    },
    modules: {
    }
})
